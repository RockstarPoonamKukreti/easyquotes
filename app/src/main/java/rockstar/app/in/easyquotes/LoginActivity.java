package rockstar.app.in.easyquotes;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import utils.ApplicationContextHolder;


public class LoginActivity extends Activity implements View.OnClickListener {
    private EditText editTextUserName, editTextPassword;
    private TextView textViewSignUP, textViewForgotPassword, textViewLogin;
    private CheckBox checkBoxRememberMe;
    private CallbackManager callbackManager;

    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private LoginButton loginButton;
    private boolean isRemembered = false;
    private ApplicationContextHolder App;
    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            AccessToken accessToken = loginResult.getAccessToken();
            Profile profile = Profile.getCurrentProfile();
            String user = profile.getName();
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            App.saveUser(user, user, "Yes");
            System.out.println("Success fully Logged in via Facebook");
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException e) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        App = ApplicationContextHolder.getAppInstance();
try{
        if (!TextUtils.isEmpty(App.wasRememberMeClicked())) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
        }}catch (NullPointerException ne) {
    ne.printStackTrace();
}

        setContentView(R.layout.activity_login);
        loginButton = (LoginButton) findViewById(R.id.login_button);
        editTextUserName = (EditText) findViewById(R.id.activity_main_edittext_username);
        editTextPassword = (EditText) findViewById(R.id.activity_main_edittext_password);
        textViewSignUP = (TextView) findViewById(R.id.activity_main_textview_sign_up);
        textViewLogin = (TextView) findViewById(R.id.activity_main_textview_sign_in);
        textViewForgotPassword = (TextView) findViewById(R.id.activity_main_textview_forgot_password);
      checkBoxRememberMe = (CheckBox) findViewById(R.id.activity_main_checkbox_remember_me);

        Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
        textViewSignUP.setOnClickListener(this);
        textViewForgotPassword.setOnClickListener(this);
      checkBoxRememberMe.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);

        printHashKey();

        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken newToken) {

            }
        };

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {
            }
        };

        accessTokenTracker.startTracking();
        profileTracker.startTracking();
        loginButton.setReadPermissions("user_friends");
        loginButton.registerCallback(callbackManager, callback);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


          case R.id.activity_main_checkbox_remember_me:
                if (!isRemembered) {
                    isRemembered = true;
                } else {
                    isRemembered = false;
                }

                break;
            case R.id.activity_main_textview_forgot_password:
                break;
            case R.id.activity_main_textview_sign_in:

                VerifyAndSignIn();
                break;

            case R.id.activity_main_textview_sign_up:

                Intent i = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(i);

                break;
        }
    }

    private void VerifyAndSignIn() {
        String username = editTextUserName.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String isrememberClicked = "No";


        if (isRemembered) {
            isrememberClicked = "Yes";
        }

        //Do Other validations Here


        if (!TextUtils.isEmpty(username)) {
           editTextUserName.setText("Please enter your username!");
        }

        if (!TextUtils.isEmpty(password)) {
            editTextUserName.setText("Please enter your password!");
        }
        if (username.equals(App.getUsername()) && password.equals(App.getPassword())) {
            Intent i = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(i);
            App.saveUser(username, password, isrememberClicked);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

    }


    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
    }

    @Override
    public void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
    }

    public void printHashKey() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "rockstar.app.in.easyquotes",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

}
