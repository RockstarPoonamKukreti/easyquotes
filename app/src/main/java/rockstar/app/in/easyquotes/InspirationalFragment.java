package rockstar.app.in.easyquotes;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.perk.perksdk.PerkManager;


public class InspirationalFragment extends Fragment  {
    TextView textViewQuote;
    ImageView imageViewShare;
    ListView listView;
    String textShare;
  public static String eventShareKey="c49dfc01b751a5689eb2e3131c2c684c2b9c836c";



    public InspirationalFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_inspirational, container, false);
        String[] InspirationalArray = getActivity().getResources().getStringArray(R.array.inspirationalQuotes);


        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.listview_inspirational, R.id.listview_inspirational_textview, InspirationalArray);
        listView = (ListView) rootView.findViewById(R.id.fragment_inspirational_listview);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                textViewQuote = (TextView) view.findViewById(R.id.listview_inspirational_textview);
                imageViewShare = (ImageView) view.findViewById(R.id.textview_icon_share);


                textShare = textViewQuote.getText().toString();

                imageViewShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PerkManager.trackEvent(getActivity(), MainActivity.key, eventShareKey, false, null);
                        inItShare();
                    }
                });


            }
        });

        return rootView;
    }

    private void inItShare() {
        //PerkManager.trackEvent(getActivity(), MainActivity.key, MainActivity.eventKey, false, null);
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String shareBody = textShare;
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Easy Quotes");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }




    }


