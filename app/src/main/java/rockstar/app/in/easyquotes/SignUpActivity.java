package rockstar.app.in.easyquotes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import utils.ApplicationContextHolder;


public class SignUpActivity extends Activity implements View.OnClickListener {
    private LinearLayout linearLayoutFacebook, linearLayoutGooglePlus;
    private EditText editTextUserName, editTextPassword;
    private TextView textViewLogin, textViewIconFacebook, textViewIconGooglePlus, textViewSingUp;
    private ApplicationContextHolder App;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        App = ApplicationContextHolder.getAppInstance();
        /*linearLayoutFacebook = (LinearLayout) findViewById(R.id.activity_signup_linear_layout_facebook);
        linearLayoutGooglePlus = (LinearLayout) findViewById(R.id.activity_signup_linear_layout_google_plus);*/
        editTextUserName = (EditText) findViewById(R.id.activity_signup_edittext_username);
        editTextPassword = (EditText) findViewById(R.id.activity_signup_edittext_password);
        textViewLogin = (TextView) findViewById(R.id.activity_signup_textview_login);
        textViewSingUp = (TextView) findViewById(R.id.activity_signup_textview_sign_in);


       /* textViewIconFacebook = (TextView) findViewById(R.id.activity_signup_textview_icon_facebook);
        textViewIconGooglePlus = (TextView) findViewById(R.id.activity_signup_textview_icon_google_plus);*/

        Typeface font = Typeface.createFromAsset(getAssets(), "fontawesome-webfont.ttf");
       /* textViewIconGooglePlus.setTypeface(font);
        textViewIconFacebook.setTypeface(font);*/
        linearLayoutFacebook.setOnClickListener(this);
        linearLayoutGooglePlus.setOnClickListener(this);
        textViewLogin.setOnClickListener(this);
        textViewSingUp.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.activity_signup_textview_sign_in:
                DoVerifyAndSignUpUser();
                break;
            case R.id.activity_signup_textview_login:

                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);

                break;
        }
    }

    private void DoVerifyAndSignUpUser() {
        String username = editTextUserName.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();

        /*DO VALIDATIONS HERE*/


        App.saveUser(username, password, "No");

        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);

    }
}
