package rockstar.app.in.easyquotes;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.perk.perksdk.PerkListener;
import com.perk.perksdk.PerkManager;

public class MainActivity extends AppCompatActivity {
   public static String key = "0d81ebd42d1c6a0f3e36f7ffb850bf613fecd937";
public static String eventKey="d1f32036df2e0ab88932b35383c354b014e3365e";
    private TextView textViewTitle;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textViewTitle = (TextView) findViewById(R.id.toolbar_textview_title);

        PerkManager.startSession(MainActivity.this, key);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        textViewTitle.setText("Inspirational");
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction efragmentTransaction = fragmentManager.beginTransaction();
        efragmentTransaction.replace(R.id.activity_main_frame, new InspirationalFragment()).commit();

        final PerkListener localPerkListener = new PerkListener() {
            @Override
            public void onPerkEvent(String reason) {
               // Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_SHORT).show();
            }
        };

        PerkManager.startSession(MainActivity.this, key, localPerkListener);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {


            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {


                if (menuItem.isChecked())

                {
                    menuItem.setChecked(false);
                } else menuItem.setChecked(true);

                drawerLayout.closeDrawers();


                switch (menuItem.getItemId()) {


                    case R.id.inspirational:
                        textViewTitle.setText("Inspirational");
                      //  PerkManager.trackEvent(MainActivity.this, key, eventKey, false, null);
                        fragmentManager = getSupportFragmentManager();
                        FragmentTransaction afragmentTransaction = fragmentManager.beginTransaction();
                        afragmentTransaction.replace(R.id.activity_main_frame, new InspirationalFragment()).commit();
                        return true;
                    case R.id.happy:
                        textViewTitle.setText("Happy");

                        fragmentManager = getSupportFragmentManager();
                        FragmentTransaction bfragmentTransaction = fragmentManager.beginTransaction();
                        bfragmentTransaction.replace(R.id.activity_main_frame, new HappyFragment()).commit();
                        return true;
                    case R.id.love:
                        textViewTitle.setText("Love");

                        fragmentManager = getSupportFragmentManager();
                        FragmentTransaction cfragmentTransaction = fragmentManager.beginTransaction();
                        cfragmentTransaction.replace(R.id.activity_main_frame, new LoveFragment()).commit();
                        return true;
                    case R.id.life:
                        textViewTitle.setText("Life");

                        fragmentManager = getSupportFragmentManager();
                        FragmentTransaction dfragmentTransaction = fragmentManager.beginTransaction();
                        dfragmentTransaction.replace(R.id.activity_main_frame, new LifeFragment()).commit();
                        return true;

                    default:
                        fragmentManager = getSupportFragmentManager();
                        FragmentTransaction efragmentTransaction = fragmentManager.beginTransaction();
                        efragmentTransaction.replace(R.id.activity_main_frame, new InspirationalFragment()).commit();                        return true;

                }
            }
        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        actionBarDrawerToggle.syncState();



    }

}

