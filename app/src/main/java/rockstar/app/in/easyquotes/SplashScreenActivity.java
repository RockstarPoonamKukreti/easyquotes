
package rockstar.app.in.easyquotes;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashScreenActivity extends Activity {


    private static int SPLASH_TIME_OUT = 2000;
    Animation animFadein, animRotate,animBounce,animFlip,animTogether;


    ImageView imageViewLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        imageViewLogo = (ImageView) findViewById((R.id.activity_splash_screen_icon_quotes));


        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        animRotate = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.rotate);
        animBounce = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.bounce);

        animFlip = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.flip);
        animTogether = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.together);

        imageViewLogo.startAnimation(animRotate);


        new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {

                Intent i = new Intent(SplashScreenActivity.this, MainActivity.class);

                startActivity(i);

                finish();
            }

        }, SPLASH_TIME_OUT);
    }
}